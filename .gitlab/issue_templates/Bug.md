## **Qual é o problema?**
Descreva com detalhes qual é o problema e qual o comportamento esperado.

## **Como podemos lidar com este _bug_?**
Nos conte como podemos lidar com esse _bug_ para consertarmos o mais rápido possível.

## **Quem pode resolver esta _issue_?**
Marque alguém que você acha que pode te ajudar a resolver essa _issue_!
