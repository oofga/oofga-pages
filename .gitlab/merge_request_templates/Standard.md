#### Referência para _issue_ :
Link de referência para _issue_ resolvida.

#### Qual o propósito deste _Merge Request_?
Descreva o porquê deste _Merge Request_ ter sido criado.

#### O que foi feito para alcançar esse propósito?
Conte-nos o que foi feito no código para resolver a _issue_ em questão.

#### Como testar para ver se funciona?
Explique brevemente como podemos testar seu _merge request_.

#### Quem pode te ajudar revisando este _Merge Request_?
Marque alguém para revisar este _merge request_.